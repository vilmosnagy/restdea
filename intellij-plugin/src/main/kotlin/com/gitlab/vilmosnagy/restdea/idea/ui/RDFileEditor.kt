package com.gitlab.vilmosnagy.restdea.idea.ui

import com.gitlab.vilmosnagy.restdea.idea.dagger.UIScope
import com.gitlab.vilmosnagy.restdea.idea.form.RestDeaForm
import com.intellij.codeHighlighting.BackgroundEditorHighlighter
import com.intellij.openapi.fileEditor.FileEditor
import com.intellij.openapi.fileEditor.FileEditorLocation
import com.intellij.openapi.fileEditor.FileEditorState
import com.intellij.openapi.util.Key
import java.beans.PropertyChangeListener
import javax.inject.Inject
import javax.swing.JComponent

@UIScope
class RDFileEditor
@Inject constructor(
        private val mainForm: RestDeaForm
) : FileEditor {

    override fun isModified(): Boolean {
        return mainForm.changedSinceLastPoll
    }

    override fun addPropertyChangeListener(listener: PropertyChangeListener) {
    }

    override fun getName(): String {
        return "Name"
    }

    override fun setState(state: FileEditorState) {

    }

    override fun getComponent(): JComponent {
        return mainForm.`$$$getRootComponent$$$`()
    }

    override fun getPreferredFocusedComponent(): JComponent? {
        return mainForm.httpMethodField
    }

    override fun <T : Any?> getUserData(key: Key<T>): T? {
        return null
    }

    override fun selectNotify() {

    }

    override fun <T : Any?> putUserData(key: Key<T>, value: T?) {
    }

    override fun getCurrentLocation(): FileEditorLocation? {
        return null
    }

    override fun deselectNotify() {
    }

    override fun getBackgroundHighlighter(): BackgroundEditorHighlighter? {
        return null
    }

    override fun isValid(): Boolean {
        return true
    }

    override fun removePropertyChangeListener(listener: PropertyChangeListener) {
    }

    override fun dispose() {
    }
}