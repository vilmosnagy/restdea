package com.gitlab.vilmosnagy.restdea.idea.services

import com.intellij.lang.Language
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class IdeaServiceWrapper
@Inject
constructor(){

    fun findLanguagesByMimeType(mimeType: String): Collection<Language> {
        return Language.findInstancesByMimeType(mimeType)
    }
}