package com.gitlab.vilmosnagy.restdea.idea.form

import com.gitlab.vilmosnagy.restdea.idea.dagger.UIScope
import com.gitlab.vilmosnagy.restdea.model.pojo.Response
import com.gitlab.vilmosnagy.restdea.idea.services.JetBrainsService
import com.gitlab.vilmosnagy.restdea.idea.util.replaceContent
import com.gitlab.vilmosnagy.restdea.model.pojo.enum.ContentType
import com.intellij.openapi.project.Project
import java.awt.BorderLayout
import java.awt.Component
import javax.inject.Inject
import javax.swing.JComponent

@UIScope
class ResultTab
@Inject constructor(
        private val jetBrainsService: JetBrainsService,
        private val _project: Project
) : GeneratedResultTab(), FormTab {

    override lateinit var requestHolder: RequestHolder
    override val enabled: Boolean = true
    override val rootPanel: Component = resultTabDetail

    init {
        responseContentTypeField.addItem(null)
        responseContentTypeField.addActionListener { responseContentTypeFieldChanged() }
        enumValues<ContentType>().forEach { responseContentTypeField.addItem(it) }
    }

    override fun refreshUI() {

    }

    private fun responseContentTypeFieldChanged() {
        TODO("See issue #16")
    }

    fun httpQueryFinished(httpResponse: Response) {
        val component = createEditorFor(httpResponse)
        resultPanel.replaceContent(component, BorderLayout.CENTER)

        statusCodeLabel.text = httpResponse.responseCode.toString()
        timeSpentLabel.text = "${httpResponse.elapsedTime.seconds} s"
        responseSizeLabel.text = "${httpResponse.content.toByteArray().size} bytes"
    }

    private fun createEditorFor(httpResponse: Response): JComponent {
        return jetBrainsService.createEditorFor(
                httpResponse.headers["Content-Type"],
                httpResponse.content,
                false
        ).component
    }

}