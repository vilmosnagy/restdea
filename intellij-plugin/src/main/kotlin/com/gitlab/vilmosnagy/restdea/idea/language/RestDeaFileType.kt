package com.gitlab.vilmosnagy.restdea.idea.language

import com.intellij.openapi.fileTypes.LanguageFileType
import javax.inject.Inject
import javax.swing.Icon

class RestDeaFileType
@Inject constructor(
    private val language: RestDeaLanguage,
    private val icon: Icon
): LanguageFileType(language) {
    
    override fun getIcon(): Icon = icon
    override fun getName(): String = "RestDea file"
    override fun getDefaultExtension(): String = "rsd"
    override fun getDescription(): String = "RestDea description"

}