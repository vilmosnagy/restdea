package com.gitlab.vilmosnagy.restdea.idea.enums

import com.gitlab.vilmosnagy.restdea.model.pojo.RequestBody
import com.gitlab.vilmosnagy.restdea.model.pojo.RequestBody.PlainRequestBody
import kotlin.reflect.KClass

enum class BodyTypes(
        val display: String,
        val type: KClass<out RequestBody>
) {

    PLAIN("plain", PlainRequestBody::class);

    override fun toString(): String {
        return display
    }
}