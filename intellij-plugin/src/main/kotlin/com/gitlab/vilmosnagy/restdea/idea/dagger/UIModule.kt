package com.gitlab.vilmosnagy.restdea.idea.dagger

import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFile
import dagger.Module
import dagger.Provides

@Module
data class UIModule(
        @get:Provides
        val project: Project,

        @get:Provides
        val file: VirtualFile
)