package com.gitlab.vilmosnagy.restdea.idea.util

import com.intellij.openapi.application.ApplicationManager
import java.util.concurrent.CompletableFuture

inline fun <T> CompletableFuture<T>.whenCompleteOnUI(crossinline done: (T?, Throwable?) -> Unit) {
    this.whenComplete { result, exception ->
        ApplicationManager.getApplication().invokeLater {
            done(result, exception)
        }
    }
}