package com.gitlab.vilmosnagy.restdea.idea.pojos

import com.gitlab.vilmosnagy.restdea.idea.form.FormTab
import com.gitlab.vilmosnagy.restdea.model.pojo.enum.HttpMethods
import javax.swing.JPanel

data class FormTabDescriptor(

        val index: Int,
        val parentPanel: JPanel,
        val tabInstance: FormTab
)