package com.gitlab.vilmosnagy.restdea.idea.language

import com.gitlab.vilmosnagy.restdea.idea.dagger.IdeaContext
import com.intellij.openapi.fileTypes.FileTypeConsumer
import com.intellij.openapi.fileTypes.FileTypeFactory

class RestDeaFileTypeFactory: FileTypeFactory() {
    override fun createFileTypes(consumer: FileTypeConsumer) {
        consumer.consume(IdeaContext.INSTANCE.restDeaFileType, "rsd")
    }
}