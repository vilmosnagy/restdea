package com.gitlab.vilmosnagy.restdea.idea.form

import com.gitlab.vilmosnagy.restdea.model.pojo.Request

interface RequestHolder {
    val request: Request

    fun updateRequest(remapFunction: (Request) -> Request)
}