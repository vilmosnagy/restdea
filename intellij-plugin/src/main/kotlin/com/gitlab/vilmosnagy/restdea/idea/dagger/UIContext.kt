package com.gitlab.vilmosnagy.restdea.idea.dagger

import com.gitlab.vilmosnagy.restdea.idea.ui.RDFileEditor
import dagger.Subcomponent
import java.lang.annotation.RetentionPolicy
import javax.inject.Scope


@UIScope
@Subcomponent(modules = arrayOf(UIModule::class))
interface UIContext {

    val fileEditor: RDFileEditor
}


@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class UIScope