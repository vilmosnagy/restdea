package com.gitlab.vilmosnagy.restdea.idea.form

import com.gitlab.vilmosnagy.restdea.model.pojo.Request
import java.awt.Component
import kotlin.reflect.KMutableProperty0

interface FormTab {
    var requestHolder: RequestHolder
    val enabled: Boolean
    val rootPanel: Component

    fun refreshUI()
}