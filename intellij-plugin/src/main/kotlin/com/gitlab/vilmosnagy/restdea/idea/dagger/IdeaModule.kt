package com.gitlab.vilmosnagy.restdea.idea.dagger

import com.gitlab.vilmosnagy.restdea.model.RequestIOService
import com.gitlab.vilmosnagy.restdea.model.dagger.ModelContext
import com.gitlab.vilmosnagy.restdea.service.dagger.ServiceContext
import com.gitlab.vilmosnagy.snakeyaml.parser.YamlParser
import com.gitlab.vilmosnagy.snakeyaml.parser.YamlSerializer
import com.gitlab.vilmosnagy.snakeyaml.parser.dagger.ParserContext
import com.intellij.openapi.util.IconLoader
import dagger.Module
import dagger.Provides
import javax.swing.Icon

@Module
class IdeaModule {

    @get:Provides
    val iconProvider = IconLoader.getIcon("/com/gitlab/vilmosnagy/restdea/idea/icons/jar-gray.png")

    @get:Provides
    val requestIOService = ModelContext.INSTANCE.requestIOService

    @get:Provides
    val requestRunnerString = ServiceContext.INSTANCE.requestRunnerService
}