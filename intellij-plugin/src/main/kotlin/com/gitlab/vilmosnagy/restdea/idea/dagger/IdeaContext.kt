package com.gitlab.vilmosnagy.restdea.idea.dagger

import com.gitlab.vilmosnagy.restdea.idea.language.RestDeaFileType
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(IdeaModule::class))
interface IdeaContext {

    companion object {
        val INSTANCE: IdeaContext = DaggerIdeaContext.builder().build()
    }

    fun plus(uiModule: UIModule): UIContext

    val restDeaFileType: RestDeaFileType
}

