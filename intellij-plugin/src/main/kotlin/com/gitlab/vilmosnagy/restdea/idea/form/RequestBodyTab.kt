package com.gitlab.vilmosnagy.restdea.idea.form

import com.gitlab.vilmosnagy.restdea.idea.dagger.UIScope
import com.gitlab.vilmosnagy.restdea.idea.enums.BodyTypes
import com.gitlab.vilmosnagy.restdea.idea.services.JetBrainsService
import com.gitlab.vilmosnagy.restdea.idea.util.replaceContent
import com.gitlab.vilmosnagy.restdea.model.pojo.RequestBody
import com.gitlab.vilmosnagy.restdea.model.pojo.enum.ContentType
import com.gitlab.vilmosnagy.restdea.model.pojo.enum.HttpMethods
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.event.DocumentListener
import java.awt.BorderLayout
import java.awt.Component
import javax.inject.Inject

@UIScope
class RequestBodyTab
@Inject constructor(
        private val jetBrainsService: JetBrainsService
) : GeneratedRequestBodyTab(), FormTab {
    override lateinit var requestHolder: RequestHolder
    override val enabled: Boolean get() = isEnabled()
    override val rootPanel: Component = tabContent

    private lateinit var editor: Editor

    init {
        enumValues<BodyTypes>().forEach { bodyTypeDropDown.addItem(it) }
        bodyTypeDropDown.addActionListener { bodyTypeValueChanged() }

        enumValues<ContentType>().forEach { plainBodyContentType.addItem(it) }
        plainBodyContentType.addActionListener { plainRequestContentTypeChanged() }
    }

    private fun plainRequestContentTypeChanged() {
        val selectedContentType = plainBodyContentType.selectedItem as ContentType
        val requestBody =  requestHolder.request.body as RequestBody.PlainRequestBody
        val newRequestBody = requestBody.copy(contentType = selectedContentType.asString)
        requestHolder.updateRequest { it.copy(body = newRequestBody) }
        refreshUI()
    }

    private fun bodyTypeValueChanged() {
        val newBodyType = bodyTypeDropDown.selectedItem as BodyTypes
        if (newBodyType.type.isInstance(requestHolder.request.body)) {

        } else {
            TODO("Request body type changed!")
        }
    }

    override fun refreshUI() {
        if (isEnabled()) {
            val body = requestHolder.request.body
            when(body) {
                is RequestBody.PlainRequestBody -> createPlainRequestEditor(body)
            }
        } else {
            requestHolder.updateRequest { it.copy(body = RequestBody.PlainRequestBody()) }
        }
    }

    private fun createPlainRequestEditor(requestBody: RequestBody.PlainRequestBody) {
        val requestContent = requestBody.content
        val contentType = requestBody.contentType
        editor = jetBrainsService.createEditorFor(contentType, requestContent, editable = true)
        editor.document.addDocumentListener(object : DocumentListener {
            override fun documentChanged(event: com.intellij.openapi.editor.event.DocumentEvent) {
                updateRequestWithBody(requestBody.copy(content = event.document.text))
            }
        })
        plainBodyDetails.isEnabled = true
        plainBodyDetails.isVisible = true
        editorPanel.replaceContent(editor.component, BorderLayout.CENTER)
    }

    private fun updateRequestWithBody(requestBody: RequestBody) {
        requestHolder.updateRequest { it.copy(body = requestBody) }
    }

    private fun isEnabled(): Boolean {
        return requestHolder.request.httpMethod in setOf(HttpMethods.POST, HttpMethods.PUT)
    }

}