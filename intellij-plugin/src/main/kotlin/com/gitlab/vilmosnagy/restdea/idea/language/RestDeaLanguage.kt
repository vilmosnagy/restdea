package com.gitlab.vilmosnagy.restdea.idea.language;

import com.intellij.lang.Language
import com.intellij.lang.xml.XMLLanguage
import javax.inject.Inject

// TODO this should inherit from the YamlLanguage, or something similar. See issue #17
class RestDeaLanguage
@Inject constructor(

) : Language("RestDea", "application/yaml") {


}
