package com.gitlab.vilmosnagy.restdea.idea.ui

import com.gitlab.vilmosnagy.restdea.idea.dagger.IdeaContext
import com.gitlab.vilmosnagy.restdea.idea.dagger.UIModule
import com.intellij.openapi.fileEditor.*
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFile

class RDEditorProvider : FileEditorProvider {

    override fun accept(project: Project, file: VirtualFile): Boolean {
        return file.extension?.equals("rsd") ?: false
    }

    override fun getPolicy(): FileEditorPolicy {
        return FileEditorPolicy.PLACE_BEFORE_DEFAULT_EDITOR
    }

    override fun getEditorTypeId(): String {
        return "restDea-editor"
    }

    override fun createEditor(project: Project, file: VirtualFile): FileEditor {
        return IdeaContext
                .INSTANCE
                .plus(UIModule(project, file))
                .fileEditor
    }
}