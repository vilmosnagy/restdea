package com.gitlab.vilmosnagy.restdea.idea.util

import java.awt.Component
import javax.swing.JPanel
import javax.swing.event.DocumentEvent
import javax.swing.event.DocumentListener
import javax.swing.text.Document

internal fun JPanel.replaceContent(component: Component, constraints: Any) {
    removeAll()
    add(component, constraints)
}

fun Document.addChangeListener(listener: (DocumentEvent) -> Unit): Unit {
    this.addDocumentListener(object : DocumentListener {
        override fun changedUpdate(e: DocumentEvent) {
            listener(e)
        }

        override fun insertUpdate(e: DocumentEvent) {
            listener(e)
        }

        override fun removeUpdate(e: DocumentEvent) {
            listener(e)
        }
    })
}