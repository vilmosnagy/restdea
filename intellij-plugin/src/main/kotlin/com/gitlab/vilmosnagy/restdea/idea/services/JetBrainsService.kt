package com.gitlab.vilmosnagy.restdea.idea.services

import com.gitlab.vilmosnagy.restdea.idea.dagger.UIScope
import com.intellij.lang.Language
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.EditorFactory
import com.intellij.openapi.fileTypes.PlainTextLanguage
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFileFactory
import javax.inject.Inject
import javax.swing.JComponent

@UIScope
class JetBrainsService
@Inject constructor(
        private val languageService: LanguageService,
        private val project: Project
) {

    fun createEditorFor(mimeType: String?, response: String, editable: Boolean): Editor {
        val language: Language = languageService.findLanguageByContentType(mimeType) ?: PlainTextLanguage.INSTANCE
        val f = PsiFileFactory.getInstance(project).createFileFromText(language, response)
        val editor = EditorFactory.getInstance().createEditor(f.viewProvider.document!!, project, language.associatedFileType!!, !editable)
        return editor
    }
}