package com.gitlab.vilmosnagy.restdea.idea.services

import com.intellij.lang.Language
import com.intellij.openapi.fileTypes.PlainTextLanguage
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LanguageService
@Inject
constructor(
        private val idea: IdeaServiceWrapper
) {

    fun findLanguageByContentType(contentType: String?): Language? {
        return when (contentType) {
            null -> PlainTextLanguage.INSTANCE
            else -> findLanguageByNotNullContentType(contentType)
        }
    }

    private fun findLanguageByNotNullContentType(contentType: String): Language {
        val language = idea.findLanguagesByMimeType(contentType).firstOrNull()
        if (language != null) {
            return language
        }

        var splittedLanguage: Language = PlainTextLanguage.INSTANCE
        if (contentType.contains(';')) {
            contentType.split(';').stream().map { findLanguageByContentType(it) }.firstOrNull()?.let {
                splittedLanguage = it
            }
        }

        return splittedLanguage
    }
}