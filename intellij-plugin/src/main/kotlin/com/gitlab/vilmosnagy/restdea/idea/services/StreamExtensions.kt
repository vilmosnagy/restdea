package com.gitlab.vilmosnagy.restdea.idea.services

import java.util.stream.Stream

fun <T> Stream<out T>.firstOrNull() = findFirst().orElse(null)