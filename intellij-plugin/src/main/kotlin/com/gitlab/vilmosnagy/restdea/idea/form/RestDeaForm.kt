package com.gitlab.vilmosnagy.restdea.idea.form

import com.gitlab.vilmosnagy.restdea.idea.dagger.UIScope
import com.gitlab.vilmosnagy.restdea.idea.pojos.FormTabDescriptor
import com.gitlab.vilmosnagy.restdea.idea.util.addChangeListener
import com.gitlab.vilmosnagy.restdea.idea.util.replaceContent
import com.gitlab.vilmosnagy.restdea.idea.util.whenCompleteOnUI
import com.gitlab.vilmosnagy.restdea.model.RequestIOService
import com.gitlab.vilmosnagy.restdea.model.pojo.Request
import com.gitlab.vilmosnagy.restdea.model.pojo.enum.HttpMethods
import com.gitlab.vilmosnagy.restdea.service.RequestRunnerService
import com.intellij.openapi.application.runReadAction
import com.intellij.openapi.application.runWriteAction
import com.intellij.openapi.vfs.VirtualFile
import java.awt.BorderLayout
import javax.inject.Inject

@UIScope
class RestDeaForm
@Inject constructor(
        private val resultTabContent: ResultTab,
        private val requestBodyTab: RequestBodyTab,
        private val requestIOService: RequestIOService,
        private val requestRunnerService: RequestRunnerService,
        private val file: VirtualFile
) : GeneratedRestDeaForm(), RequestHolder {

    var changedSinceLastPoll: Boolean = true
    private val requestLock = Any()
    override var request: Request = Request()
        private set

    private val TABS = listOf(
            FormTabDescriptor(0, resultTab, resultTabContent),
            FormTabDescriptor(1, requestTab, requestBodyTab)
    )

    init {
        TABS.forEach { it.parentPanel.replaceContent(it.tabInstance.rootPanel, BorderLayout.CENTER) }
        enumValues<HttpMethods>().forEach { httpMethodField.addItem(it) }
        sendButton.addActionListener { doRestQuery() }
        httpMethodField.addActionListener { httpMethodFieldChanged() }
        urlField.document.addChangeListener { urlFieldChanged() }
        loadStatusFromDisk()
    }

    override fun updateRequest(remapFunction: (Request) -> Request) {
        synchronized(requestLock) {
            val newRequest = remapFunction(request)
            request = newRequest
        }
        syncChanges()
    }

    private fun loadStatusFromDisk() {
        val contentsOfFile = runReadAction { file.inputStream.reader().readText() }
        updateRequest { requestIOService.readRequest(contentsOfFile) }
        resetUIValues()
        resetTabUIValues()
    }

    private fun resetUIValues() {
        httpMethodField.selectedItem = request.httpMethod
        urlField.text = request.url
    }

    private fun urlFieldChanged() {
        updateRequest { it.copy(url = urlField.text ?: "") }
    }

    private fun syncChanges() {
        changedSinceLastPoll = true
        val contentsOfFile = requestIOService.serializeRequest(request)
        runWriteAction {
            file
                    .getOutputStream(this)
                    .use { it.write(contentsOfFile.toByteArray()) }
        }
    }

    private fun httpMethodFieldChanged() {
        updateRequest { it.copy(httpMethod = httpMethodField.selectedItem as HttpMethods) }
        resetTabUIValues()
    }

    private fun resetTabUIValues() {
        TABS.forEach {
            it.tabInstance.requestHolder = this
            it.tabInstance.refreshUI()
            mainTabbedPane.setEnabledAt(it.index, it.tabInstance.enabled)
        }
        if (!mainTabbedPane.isEnabledAt(mainTabbedPane.selectedIndex)) {
            mainTabbedPane.selectedIndex = 0
        }
        mainTabbedPane.revalidate()
    }

    private fun doRestQuery() {
        val request = this.request
        if (request.url.isNotEmpty()) {
            val queryAsync = requestRunnerService.runRequest(request)
            queryAsync.whenCompleteOnUI { response, exception ->
                when {
                    response != null -> resultTabContent.httpQueryFinished(response)
                    exception != null -> throw exception
                    else -> TODO("Handle unknown error")
                }
                mainComponent.revalidate()
            }
        }
    }
}