package com.gitlab.vilmosnagy.restdea.idea.services

import com.intellij.json.JsonLanguage
import com.intellij.lang.xml.XMLLanguage
import com.intellij.openapi.fileTypes.PlainTextLanguage
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test


internal class LanguageServiceTest {

    private val idea: IdeaServiceWrapper = mockk()

    private val testObj = LanguageService(idea)

    @Test
    fun `should return PLAIN_TEXT language type for empty mimeType`() {
        val language = testObj.findLanguageByContentType(null)
        assertEquals(PlainTextLanguage.INSTANCE, language)
    }

    @Test
    fun `should return correct language type when mimeType matches with single IDEA representation`() {
        every { idea.findLanguagesByMimeType("application/json") } returns listOf(JsonLanguage.INSTANCE)
        val language = testObj.findLanguageByContentType("application/json")
        assertEquals(JsonLanguage.INSTANCE, language)
    }

    @Test
    fun `should return correct language type when mimeType matches with multiple IDEA representation`() {
        every { idea.findLanguagesByMimeType("application/json") } returns listOf(JsonLanguage.INSTANCE, XMLLanguage.INSTANCE)
        val language = testObj.findLanguageByContentType("application/json")
        assertEquals(JsonLanguage.INSTANCE, language)
    }

    @Test
    fun `should return correct language type when mimeType contains extra info`() {
        every { idea.findLanguagesByMimeType("application/json") } returns listOf(JsonLanguage.INSTANCE, XMLLanguage.INSTANCE)
        every { idea.findLanguagesByMimeType("application/json; charset=utf-8") } returns emptyList()
        val language = testObj.findLanguageByContentType("application/json; charset=utf-8")
        assertEquals(JsonLanguage.INSTANCE, language)
    }
}