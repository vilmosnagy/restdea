package com.gitlab.vilmosnagy.restdea.service.dagger

import com.gitlab.vilmosnagy.restdea.model.dagger.ModelContext
import com.gitlab.vilmosnagy.restdea.model.dagger.ModelModule
import com.gitlab.vilmosnagy.restdea.service.RequestRunnerService
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ServiceModule::class))
interface ServiceContext {

    companion object {
        val INSTANCE: ServiceContext = DaggerServiceContext.builder().build()
    }

    val requestRunnerService: RequestRunnerService
}