package com.gitlab.vilmosnagy.restdea.service

import com.gitlab.vilmosnagy.restdea.model.pojo.Request
import com.gitlab.vilmosnagy.restdea.model.pojo.RequestBody
import com.gitlab.vilmosnagy.restdea.model.pojo.Response
import java.net.HttpURLConnection
import java.net.URL
import java.time.Instant
import java.util.concurrent.CompletableFuture
import javax.inject.Inject

class RequestRunnerService
@Inject constructor(

) {

    fun runRequest(request: Request): CompletableFuture<Response> {
        return CompletableFuture.supplyAsync {
            val startInstant = Instant.now()
            val connection = URL(request.url).openConnection() as HttpURLConnection
            connection.requestMethod = request.httpMethod.toString()
            connection.readTimeout = 15_000
            connection.writeOutput(request.body)
            connection.connect()
            return@supplyAsync Response.getInstance(connection, startInstant)
        }
    }
}

private fun HttpURLConnection.writeOutput(body: RequestBody?) {
    when (body) {
        is RequestBody.PlainRequestBody -> writeOutput(body)
    }
}

private fun HttpURLConnection.writeOutput(body: RequestBody.PlainRequestBody) {
    if (getRequestProperty("Content-Type") == null) {
        setRequestProperty("Content-Type", body.contentType)
    }
    doOutput = true
    outputStream.write(body.content.toByteArray())
}
