package com.gitlab.vilmosnagy.restdea.model

fun <K, V> Map<K, V?>.filterNotNullValues(): Map<K, V> {
    return this.filterValues { it != null } as Map<K, V>
}