package com.gitlab.vilmosnagy.restdea.model.pojo.yamlfactories

import com.gitlab.vilmosnagy.restdea.model.pojo.SimpleParameter
import kotlin.reflect.KFunction

internal class SimpleParameterFactory : AbstractKeyValueMinusTypeFactory<SimpleParameter>() {
    override fun serializeObject(obj: SimpleParameter?): Map<String, Any> {
        return if (obj == null) emptyMap()
        else mapOf(
                "type" to "simple",
                obj.name to obj.getValue()
        )
    }

    override fun getConstructor(): KFunction<SimpleParameter> = ::SimpleParameter
}