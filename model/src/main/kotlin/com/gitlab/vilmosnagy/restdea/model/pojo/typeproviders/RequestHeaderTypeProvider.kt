package com.gitlab.vilmosnagy.restdea.model.pojo.typeproviders

import com.gitlab.vilmosnagy.restdea.model.pojo.RequestHeader
import com.gitlab.vilmosnagy.restdea.model.pojo.SimpleRequestHeader
import com.gitlab.vilmosnagy.snakeyaml.parser.interfaces.YamlObjectTypeProvider
import kotlin.reflect.KClass

class RequestHeaderTypeProvider : YamlObjectTypeProvider<RequestHeader> {
    override fun provideResultClass(yamlAsMap: Map<String, Any>): KClass<out RequestHeader> {
        return when(yamlAsMap["type"]) {
            "simple" -> SimpleRequestHeader::class
            else -> TODO()
        }
    }

}