package com.gitlab.vilmosnagy.restdea.model.pojo

interface RequestHeader {
    fun getKey(): String
    fun getValue(): String
}

interface Parameter {
    val name: String
    fun getValue(): Any
}
