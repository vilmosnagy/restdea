package com.gitlab.vilmosnagy.restdea.model.pojo

import com.gitlab.vilmosnagy.restdea.model.pojo.yamlfactories.SimpleParameterFactory
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlFactory

@YamlFactory(SimpleParameterFactory::class)
data class SimpleParameter(
        override val name: String,
        private val value: String
): Parameter {
    override fun getValue() = value
}

