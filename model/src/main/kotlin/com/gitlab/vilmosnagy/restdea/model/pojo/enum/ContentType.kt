package com.gitlab.vilmosnagy.restdea.model.pojo.enum

enum class ContentType(
        val asString: String
) {

    JSON("application/json"),
    XML("text/xml"),
    HTML("text/html");


    override fun toString() = asString
}