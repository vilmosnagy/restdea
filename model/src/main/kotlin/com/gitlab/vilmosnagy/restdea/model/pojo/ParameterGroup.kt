package com.gitlab.vilmosnagy.restdea.model.pojo

import com.gitlab.vilmosnagy.restdea.model.pojo.typeproviders.ParameterTypeProvider
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.*
import com.gitlab.vilmosnagy.snakeyaml.parser.interfaces.YamlObjectFactory

data class ParameterGroup
@YamlConstructor constructor(
        @YamlElement(name = "name")
        val name: String,

        @YamlCollection(name = "parameters")
        @YamlTypeProvider(ParameterTypeProvider::class)
        val parameters: Set<Parameter>,

        @YamlElement(name = "testCase")
        val test: String
)