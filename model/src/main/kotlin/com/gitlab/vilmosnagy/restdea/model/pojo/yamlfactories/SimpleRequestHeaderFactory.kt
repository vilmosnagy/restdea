package com.gitlab.vilmosnagy.restdea.model.pojo.yamlfactories

import com.gitlab.vilmosnagy.restdea.model.pojo.SimpleRequestHeader
import kotlin.reflect.KFunction

internal class SimpleRequestHeaderFactory : AbstractKeyValueMinusTypeFactory<SimpleRequestHeader>() {
    override fun serializeObject(obj: SimpleRequestHeader?): Map<String, Any> {
        return if (obj == null) emptyMap()
        else mapOf(
                "type" to "simple",
                obj.getKey() to obj.getValue()
        )
    }

    override fun getConstructor(): KFunction<SimpleRequestHeader> = ::SimpleRequestHeader
}
