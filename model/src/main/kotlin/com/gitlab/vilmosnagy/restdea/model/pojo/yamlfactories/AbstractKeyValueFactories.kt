package com.gitlab.vilmosnagy.restdea.model.pojo.yamlfactories

import com.gitlab.vilmosnagy.snakeyaml.parser.interfaces.YamlObjectFactory
import kotlin.reflect.KFunction

internal abstract class AbstractKeyValueMinusTypeFactory<T : Any> : YamlObjectFactory<T> {

    abstract fun getConstructor(): KFunction<T>

    final override fun createObject(yamlAsMap: Map<String, Any>): T {
        val mapWithoutType = yamlAsMap - "type"
        if (mapWithoutType.size != 1) {
            TODO()
        } else {
            val entry = mapWithoutType.entries.first()
            return getConstructor().call(entry.key, entry.value.toString())
        }
    }
}
