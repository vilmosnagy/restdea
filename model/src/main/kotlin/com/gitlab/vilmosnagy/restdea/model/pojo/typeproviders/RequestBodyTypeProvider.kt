package com.gitlab.vilmosnagy.restdea.model.pojo.typeproviders

import com.gitlab.vilmosnagy.restdea.model.pojo.RequestBody
import com.gitlab.vilmosnagy.snakeyaml.parser.interfaces.YamlObjectTypeProvider
import kotlin.reflect.KClass

internal class RequestBodyTypeProvider : YamlObjectTypeProvider<RequestBody> {
    override fun provideResultClass(yamlAsMap: Map<String, Any>): KClass<out RequestBody> {
        return when(yamlAsMap["type"]) {
            null -> RequestBody.PlainRequestBody::class
            "plain" -> RequestBody.PlainRequestBody::class
            else -> TODO()
        }
    }

}