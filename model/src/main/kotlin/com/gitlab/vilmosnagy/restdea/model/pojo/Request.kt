package com.gitlab.vilmosnagy.restdea.model.pojo

import com.gitlab.vilmosnagy.restdea.model.pojo.RequestBody.PlainRequestBody
import com.gitlab.vilmosnagy.restdea.model.pojo.enum.ContentType
import com.gitlab.vilmosnagy.restdea.model.pojo.enum.HttpMethods
import com.gitlab.vilmosnagy.restdea.model.pojo.typeproviders.RequestBodyTypeProvider
import com.gitlab.vilmosnagy.restdea.model.pojo.typeproviders.RequestHeaderTypeProvider
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlCollection
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlElement
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlTypeProvider

data class Request @YamlConstructor constructor(

        @YamlElement(name = "httpMethod")
        val httpMethod: HttpMethods,

        @YamlElement(name = "url")
        val url: String,

        @YamlElement(name = "body")
        @YamlTypeProvider(RequestBodyTypeProvider::class)
        val body: RequestBody?,

        @YamlCollection(name = "headers")
        @YamlTypeProvider(RequestHeaderTypeProvider::class)
        val headers: List<RequestHeader>,

        @YamlCollection(name = "parameterGroups")
        val parameterGroups: List<ParameterGroup>
) {
        constructor(): this(HttpMethods.GET,  "", PlainRequestBody(), emptyList(), emptyList())
}