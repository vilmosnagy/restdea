package com.gitlab.vilmosnagy.restdea.model.pojo

import java.net.HttpURLConnection
import java.time.Duration
import java.time.Instant

data class Response private constructor(
        val content: String,
        val headers: Map<String, String>,
        val responseCode: Int,
        val elapsedTime: Duration
) {

    companion object {
        fun getInstance(connection: HttpURLConnection, startInstant: Instant): Response {
            val duration = Duration.between(startInstant, Instant.now())
            val responseCode = connection.responseCode
            val content = when (responseCode) {
                in 200..300 -> connection.inputStream.reader().readText()
                else -> connection.errorStream.reader().readText()
            }
            return Response(
                    content,
                    connection.headerFields.map { (k, v) -> k to v.joinToString() }.toMap(),
                    responseCode,
                    duration
            )
        }
    }

}