package com.gitlab.vilmosnagy.restdea.model.pojo

import com.gitlab.vilmosnagy.restdea.model.pojo.yamlfactories.SimpleRequestHeaderFactory
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlFactory

@YamlFactory(SimpleRequestHeaderFactory::class)
internal data class SimpleRequestHeader(
        private val key: String,
        private val value: String
) : RequestHeader {
    override fun getKey() = key
    override fun getValue() = value
}

