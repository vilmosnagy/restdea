package com.gitlab.vilmosnagy.restdea.model.pojo

import com.gitlab.vilmosnagy.restdea.model.pojo.yamlfactories.PlainRequestBodyFactory
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlElement
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlFactory

sealed class RequestBody {

    @YamlFactory(PlainRequestBodyFactory::class)
    data class PlainRequestBody @YamlConstructor constructor(

            @YamlElement("contentType")
            val contentType: String,

            @YamlElement("content")
            val content: String
    ): RequestBody() {

        constructor(): this("application/json", "")
    }
}