package com.gitlab.vilmosnagy.restdea.model.dagger

import com.gitlab.vilmosnagy.restdea.model.RequestIOService
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ModelModule::class))
interface ModelContext {

    companion object {
        val INSTANCE: ModelContext = DaggerModelContext.builder().build()
    }

    val requestIOService: RequestIOService
}
