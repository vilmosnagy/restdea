package com.gitlab.vilmosnagy.restdea.model.dagger

import com.gitlab.vilmosnagy.snakeyaml.parser.YamlParser
import com.gitlab.vilmosnagy.snakeyaml.parser.YamlSerializer
import com.gitlab.vilmosnagy.snakeyaml.parser.dagger.ParserContext
import dagger.Module
import dagger.Provides

@Module
class ModelModule {

    @get:Provides
    val yamlParser: YamlParser = ParserContext.INSTANCE.yamlParser

    @get:Provides
    val yamlSerializer: YamlSerializer = ParserContext.INSTANCE.yamlSerializer

}