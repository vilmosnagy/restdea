package com.gitlab.vilmosnagy.restdea.model.pojo.yamlfactories

import com.gitlab.vilmosnagy.restdea.model.filterNotNullValues
import com.gitlab.vilmosnagy.restdea.model.pojo.RequestBody
import com.gitlab.vilmosnagy.snakeyaml.parser.dagger.ParserContext
import com.gitlab.vilmosnagy.snakeyaml.parser.interfaces.YamlObjectFactory

internal class PlainRequestBodyFactory: YamlObjectFactory<RequestBody.PlainRequestBody> {
    override fun serializeObject(obj: RequestBody.PlainRequestBody?): Map<String, Any> {
        if (obj == null) {
            return emptyMap()
        }

        return mapOf(
                "type" to "plain",
                "contentType" to obj.contentType,
                "content" to obj.content
        )
    }

    override fun createObject(yamlAsMap: Map<String, Any>): RequestBody.PlainRequestBody {
        return RequestBody.PlainRequestBody(
                yamlAsMap["contentType"].toString(),
                yamlAsMap["content"].toString()
        )
    }

}