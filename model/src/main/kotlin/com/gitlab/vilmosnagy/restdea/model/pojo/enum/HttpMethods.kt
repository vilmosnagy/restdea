package com.gitlab.vilmosnagy.restdea.model.pojo.enum

enum class HttpMethods {

    GET,
    POST,
    PUT,
    HEAD,
    DELETE

}