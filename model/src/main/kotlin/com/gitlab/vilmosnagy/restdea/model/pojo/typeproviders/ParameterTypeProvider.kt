package com.gitlab.vilmosnagy.restdea.model.pojo.typeproviders

import com.gitlab.vilmosnagy.restdea.model.pojo.Parameter
import com.gitlab.vilmosnagy.restdea.model.pojo.SimpleParameter
import com.gitlab.vilmosnagy.snakeyaml.parser.interfaces.YamlObjectTypeProvider
import kotlin.reflect.KClass

internal class ParameterTypeProvider: YamlObjectTypeProvider<Parameter> {
    override fun provideResultClass(yamlAsMap: Map<String, Any>): KClass<out Parameter> {
        return when(yamlAsMap["type"]) {
            "simple" -> SimpleParameter::class
            else -> TODO()
        }
    }

}