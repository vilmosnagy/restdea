package com.gitlab.vilmosnagy.restdea.model

import com.gitlab.vilmosnagy.restdea.model.pojo.Request
import com.gitlab.vilmosnagy.snakeyaml.parser.YamlParser
import com.gitlab.vilmosnagy.snakeyaml.parser.YamlSerializer
import java.io.StringReader
import javax.inject.Inject
import javax.xml.bind.Unmarshaller

class RequestIOService
@Inject constructor(
        private val yamlParser: YamlParser,
        private val yamlSerializer: YamlSerializer
) {

    fun readRequest(requestAsString: String): Request {
        return yamlParser.parseYaml(requestAsString, Request::class)
    }

    fun serializeRequest(request: Request): String {
        return yamlSerializer.serializeYaml(request)
    }

}