package com.gitlab.vilmosnagy.restdea.model

import com.gitlab.vilmosnagy.restdea.get
import com.gitlab.vilmosnagy.restdea.junit.ReadResource
import com.gitlab.vilmosnagy.restdea.junit.ResourceReaderExtension
import com.gitlab.vilmosnagy.restdea.model.dagger.ModelContext
import com.gitlab.vilmosnagy.restdea.model.pojo.Request
import com.gitlab.vilmosnagy.restdea.model.pojo.RequestBody
import com.gitlab.vilmosnagy.restdea.model.pojo.SimpleParameter
import com.gitlab.vilmosnagy.restdea.model.pojo.SimpleRequestHeader
import com.gitlab.vilmosnagy.restdea.model.pojo.enum.HttpMethods
import org.json.JSONObject
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.yaml.snakeyaml.Yaml


@ExtendWith(ResourceReaderExtension::class)
internal class RequestIOServiceTest {

    private val testObj = ModelContext.INSTANCE.requestIOService

    @Test
    fun `should read basic rest request from string`(@ReadResource requestAsString: String) {
        val request = testObj.readRequest(requestAsString)
        assertEquals(HttpMethods.GET, request.httpMethod)
        assertEquals("\$baseUrl", request.url)
        assertEquals(listOf(SimpleRequestHeader("Content-Type", "application/html")), request.headers)
        assertNull(request.body)
    }

    @Test
    fun `should read basic post request with body`(@ReadResource requestAsString: String) {
        val request = testObj.readRequest(requestAsString)
        assertEquals(HttpMethods.POST, request.httpMethod)
        assertEquals("\$baseUrl/books", request.url)
        assertTrue(request.body is RequestBody.PlainRequestBody)
        val requestBody = request.body as RequestBody.PlainRequestBody
        assertEquals("application/json", requestBody.contentType)
        val parsedBody = JSONObject(requestBody.content)
        assertEquals("1984", parsedBody.getString("title"))
        assertEquals("George Orwell", parsedBody.getString("author"))
    }

    @Test
    fun `should write basic post request with a body`() {
        val content = """|{
                           |  "name: "1984",
                           |  "author": "Geroge Orwell
                           |}""".trimMargin()
        val request = Request(
                HttpMethods.POST,
                "https://jsonplaceholder.typicode.com/posts",
                RequestBody.PlainRequestBody(
                        "application/json",
                        content),
                emptyList(), emptyList()
        )

        val asString = testObj.serializeRequest(request)
        val reParsedToMap = Yaml().load<Map<String, Any>>(asString)
        assertEquals("POST", reParsedToMap["httpMethod"])
        assertEquals("https://jsonplaceholder.typicode.com/posts", reParsedToMap["url"])
        assertEquals("application/json", reParsedToMap["body"]["contentType"])
        assertEquals(content, reParsedToMap["body"]["content"])
        assertEquals(emptyList<Nothing>(), reParsedToMap["headers"])
        assertEquals(emptyList<Nothing>(), reParsedToMap["parameterGroups"])
    }

    @Test
    fun `should read parameter groups correctly`(@ReadResource requestAsString: String) {
        val request = testObj.readRequest(requestAsString)
        assertEquals(HttpMethods.POST, request.httpMethod)
        assertEquals("\$baseUrl/books", request.url)
        assertEquals(listOf(SimpleRequestHeader("Content-Type", "application/json")), request.headers)
        assertTrue(request.body is RequestBody.PlainRequestBody)
        val requestBody = request.body as RequestBody.PlainRequestBody
        assertEquals("application/json", requestBody.contentType)
        val parsedBody = JSONObject(requestBody.content)
        assertEquals("\$title", parsedBody.getString("title"))
        assertEquals("\$author", parsedBody.getString("author"))

        val parameterGroups = request.parameterGroups
        val group = parameterGroups[0]
        assertEquals("Valid Create Book", group.name)
        assertEquals(setOf(SimpleParameter("title", "1984"), SimpleParameter("author", "George Orwell")), group.parameters)
        assertEquals("""tests["Request status is Created(201)"] = response.status == 201""", group.test.trim())
    }
}