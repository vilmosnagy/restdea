package com.gitlab.vilmosnagy.restdea

internal operator fun Any?.get(key: Any): Any? {
    if (this == null) {
        return null
    }

    if (this is List<*> && key is Int) {
        return this[key]
    }

    if (this is Map<*, *>) {
        return (this as Map<Any, *>).get(key)
    }

    TODO()
}