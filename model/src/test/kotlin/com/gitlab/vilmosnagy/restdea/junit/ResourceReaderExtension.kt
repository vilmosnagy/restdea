package com.gitlab.vilmosnagy.restdea.junit

import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.ParameterResolver

private const val DEFAULT_VALUE = "##default##"

@Target(AnnotationTarget.VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
annotation class ReadResource(

        /**
         * The name of the resource to read.
         */
        val value: String = DEFAULT_VALUE
)

class ResourceReaderExtension: ParameterResolver {
    override fun supportsParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Boolean {
        return getAnnotation(parameterContext) != null
    }

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Any {
        val annotationValue = getAnnotation(parameterContext)!!.value
        val resourceName = when(annotationValue) {
            DEFAULT_VALUE -> "${extensionContext.testMethod.get().name.replace(' ', '_')}.yml"
            else -> annotationValue
        }
        return readResource(resourceName, extensionContext)
    }

    private fun readResource(resourceName: String, extensionContext: ExtensionContext): String {
        val testClass = extensionContext.testClass.get()
        val inputStream = testClass.getResourceAsStream("${testClass.simpleName}/$resourceName")
        return inputStream.reader().readText()
    }

    private fun getAnnotation(parameterContext: ParameterContext): ReadResource? {
        return parameterContext.parameter.annotations.firstOrNull { it is ReadResource } as ReadResource?
    }

}