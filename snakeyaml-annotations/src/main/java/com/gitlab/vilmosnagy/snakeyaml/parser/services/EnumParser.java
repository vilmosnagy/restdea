package com.gitlab.vilmosnagy.snakeyaml.parser.services;

public class EnumParser {

    private EnumParser() {

    }

    @SuppressWarnings("unchecked")
    public static Enum<?> enumValueOf(Class<?> klass, String value) {
        return Enum.valueOf((Class) klass, value);
    }

    public static Enum<?> firstEnumValueOf(Class<?> klass) {
        return (Enum<?>) klass.getEnumConstants()[0];
    }
}
