package com.gitlab.vilmosnagy.snakeyaml.parser

import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlFactory
import com.gitlab.vilmosnagy.snakeyaml.parser.interfaces.YamlObjectFactory
import com.gitlab.vilmosnagy.snakeyaml.parser.services.AnnotationParser
import com.gitlab.vilmosnagy.snakeyaml.parser.services.getAnnotation
import com.gitlab.vilmosnagy.snakeyaml.parser.services.isAnnotationPresent
import org.yaml.snakeyaml.Yaml
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.reflect.full.createInstance

@Singleton
class YamlSerializer
@Inject constructor(
        private val annotationParser: AnnotationParser,
        private val snakeYamlParser: Yaml
) {

    fun serializeYaml(yamlObject: Any): String {
        val yamlAsMap = convertObjectToCorrectYamlMap(yamlObject)
        return snakeYamlParser.dump(yamlAsMap)
    }

    private fun convertObjectToCorrectYamlMap(yamlObject: Any): Map<String, Any?> {
        if (yamlObject::class.isAnnotationPresent(YamlFactory::class)) {
            return deserializeWithFactory(yamlObject)
        }

        val parsedAnnotation = annotationParser.parseAnnotationsOnClass(yamlObject::class)
        return parsedAnnotation
                .fields
                .map { it to it.property.getter.call(yamlObject) }
                .map { it.first to remapSpecialObjects(it.second) }
                .map { it.first.elementName to it.second }
                .toMap()
    }

    private fun remapSpecialObjects(yamlObject: Any?): Any? {
        if (yamlObject == null) {
            return null
        }

        if (yamlObject::class.constructors.any { it.isAnnotationPresent(YamlConstructor::class) }) {
            return convertObjectToCorrectYamlMap(yamlObject)
        }

        return when (yamlObject) {
            is Enum<*> -> yamlObject.name
            is Collection<*> -> unfoldCollectionsOfPojos(yamlObject)
            else -> yamlObject
        }
    }

    private fun deserializeWithFactory(yamlObject: Any): Map<String, Any?> {
        val factory: YamlObjectFactory<Any> = yamlObject::class.getAnnotation(YamlFactory::class)!!.factoryClass.createInstance() as YamlObjectFactory<Any>
        return factory.serializeObject(yamlObject)
    }

    private fun unfoldCollectionsOfPojos(yamlObject: Collection<*>): Any? {
        return yamlObject
                .filterNotNull()
                .map { convertObjectToCorrectYamlMap(it) }
                .toList()
    }
}
