package com.gitlab.vilmosnagy.snakeyaml.parser

import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlCollection
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlFactory
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlTypeProvider
import com.gitlab.vilmosnagy.snakeyaml.parser.mappers.EnumTypeMapper
import com.gitlab.vilmosnagy.snakeyaml.parser.mappers.getTypeMapperForClass
import com.gitlab.vilmosnagy.snakeyaml.parser.services.*
import org.yaml.snakeyaml.Yaml
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.KVisibility
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.isSubclassOf
import kotlin.reflect.full.isSuperclassOf
import kotlin.reflect.jvm.isAccessible

@Singleton
class YamlParser
@Inject constructor(
        private val annotationParser: AnnotationParser,
        private val snakeYamlParser: Yaml
) {

    fun <T : Any> parseYaml(yamlContent: String, klass: KClass<T>): T {
        val parsedYaml = snakeYamlParser.load<Map<String, Any>>(yamlContent) ?: emptyMap()
        return parseYaml(parsedYaml, klass)
    }

    private fun <T : Any> parseYaml(yamlAsMap: Map<String, Any>, klass: KClass<T>): T {
        val parsedAnnotation = annotationParser.parseAnnotationsOnClass(klass)
        val parameterValues = parseParametersFromMap(yamlAsMap, parsedAnnotation)
        return when {
            klass.isAnnotationPresent(YamlFactory::class) -> parseYamlWithFactory(yamlAsMap, klass)
            else -> createNullObjectWithSmartNullabilityCheck(parsedAnnotation, parameterValues)
        }
    }

    private fun <T : Any> createNullObjectWithSmartNullabilityCheck(parsedAnnotation: AnnotationParsedClass<T>, parameterValues: List<*>): T {
        val argumentsToParameters = parsedAnnotation
                .constructor!!
                .parameters
                .mapIndexed {idx, param -> param to parameterValues[idx] }
                .map { tryToCreateDefaultIfNonNullableParameterIsNull(it.first, it.second) }

        return parsedAnnotation.constructor.call(*argumentsToParameters.toTypedArray())
    }

    private fun tryToCreateDefaultIfNonNullableParameterIsNull(parameter: KParameter, value: Any?): Any? {
        if (parameter.type.isMarkedNullable) {
            return value
        } else if (value != null) {
            return value
        }

        val classifier = parameter.type.classifier
        if (classifier is KClass<*>) {
            if (classifier.isSubclassOf(Enum::class)) {
                return EnumParser.firstEnumValueOf(classifier.java)
            } else if (classifier.constructors.any { it.parameters.isEmpty()}) {
                val constructor = classifier.constructors.first { it.parameters.isEmpty() }
                constructor.isAccessible = true
                return constructor.call()
            } else {
                TODO()
            }
        } else {
            TODO()
        }
    }

    private fun <T : Any> parseYamlWithFactory(yamlAsMap: Map<String, Any>, klass: KClass<T>): T {
        val factoryAnnotation = klass.getAnnotation(YamlFactory::class)!!
        val factory = factoryAnnotation.factoryClass.createInstance()
        @Suppress("UNCHECKED_CAST")
        return factory.createObject(yamlAsMap) as T
    }

    private fun parseParametersFromMap(yamlAsMap: Map<String, Any?>, parsedAnnotation: AnnotationParsedClass<*>): List<*> {
        return parsedAnnotation
                .fields
                .map { it to yamlAsMap[it.elementName] }
                .map { it.first to mapToCorrectType(it.first, it.second) }
                .map { it.second }
    }

    private fun mapToCorrectType(parsedElement: AnnotationParsedElement, objectFromYaml: Any?): Any? {
        val klass = parsedElement.property.returnType.classifier as KClass<*>
        return when {
            parsedElement.property.isAnnotationPresent(YamlCollection::class) -> parseCollectionToCorrectType(parsedElement, objectFromYaml as Collection<*>?)
            objectFromYaml == null -> null
            parsedElement.property.isAnnotationPresent(YamlTypeProvider::class) -> parseToObjBasedOnConverter(parsedElement, castToMap(objectFromYaml))
            else -> rawMapToCorrectType(klass, objectFromYaml)
        }
    }

    private fun <T : Any> rawMapToCorrectType(klass: KClass<T>, objectFromYaml: Any): Any? {
        return getTypeMapperForClass(klass)?.convertToType(klass, objectFromYaml)
                ?: parseYaml(castToMap(objectFromYaml), klass)
    }

    private fun parseCollectionToCorrectType(parsedElement: AnnotationParsedElement, objectFromYaml: Collection<*>?): Collection<*> {
        val parameterType: KClass<Collection<*>> = parsedElement.property.returnType.classifier as KClass<Collection<*>>
        val parsedCollection: Collection<Any?>
        if (objectFromYaml == null) {
            parsedCollection = emptyList()
        } else if (parsedElement.property.isAnnotationPresent(YamlTypeProvider::class)) {
            parsedCollection = objectFromYaml.filterNotNull().map { parseToObjBasedOnConverter(parsedElement, castToMap(it)) }
        } else {
            val genericType = parsedElement.property.returnType.arguments[0].type?.classifier as? KClass<*> ?: TODO()
            parsedCollection = when {
                Collection::class.isSuperclassOf(parameterType) -> objectFromYaml.filterNotNull().map { rawMapToCorrectType(genericType, it) }
                else -> TODO()
            }
        }

        return getTypeMapperForClass(parameterType)?.convertToType(parameterType, parsedCollection) as Collection<*>
    }

    private fun parseToObjBasedOnConverter(parsedElement: AnnotationParsedElement, yamlAsMap: Map<String, Any>): Any? {
        val converterAnnotation = parsedElement.property.getAnnotation(YamlTypeProvider::class) ?: TODO()
        val typeProvider = converterAnnotation.typeProviderClass.createInstance()
        val klass = typeProvider.provideResultClass(yamlAsMap)
        return parseYaml(yamlAsMap, klass)
    }

    private fun castToMap(objectFromYaml: Any): Map<String, Any> {
        @Suppress("UNCHECKED_CAST")
        return objectFromYaml as Map<String, Any>
    }
}