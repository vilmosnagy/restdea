package com.gitlab.vilmosnagy.snakeyaml.parser.mappers

import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf
import kotlin.reflect.full.isSuperclassOf
import kotlin.reflect.full.isSupertypeOf

object ListTypeMapper: TypeMapper<List<*>> {
    override fun getType(): KClass<List<*>> = List::class

    override fun convertToType(klass: KClass<List<*>>, obj: Any): List<*> {
        if (obj !is Collection<*>) {
            TODO()
        }
        if (List::class.isSubclassOf(klass)) {
            return obj.toList()
        } else {
            return klass
                    .constructors
                    .filter { it.parameters.size == 1 }
                    .firstOrNull {  (it.parameters[0].type.classifier as? KClass<*>)?.isSuperclassOf(List::class) ?: false }
                    ?.call(obj.toList()) ?: TODO()
        }
    }
}

object SetTypeMapper: TypeMapper<Set<*>> {
    override fun getType(): KClass<Set<*>> = Set::class

    override fun convertToType(klass: KClass<Set<*>>, obj: Any): Set<*> {
        if (obj !is Collection<*>) {
            TODO()
        }
        if (Set::class.isSubclassOf(klass)) {
            return obj.toSet()
        } else {
            return klass
                    .constructors
                    .filter { it.parameters.size == 1 }
                    .firstOrNull {  (it.parameters[0].type.classifier as? KClass<*>)?.isSuperclassOf(Set::class) ?: false }
                    ?.call(obj.toSet()) ?: TODO()
        }
    }
}