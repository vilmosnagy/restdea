package com.gitlab.vilmosnagy.snakeyaml.parser.dagger

import dagger.Module
import dagger.Provides
import org.yaml.snakeyaml.Yaml
import javax.inject.Singleton

@Module
class ParserModule {

    @Provides
    @Singleton
    fun snakeYamlParser() = Yaml()

}