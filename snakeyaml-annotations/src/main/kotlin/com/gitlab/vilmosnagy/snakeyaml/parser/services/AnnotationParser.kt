package com.gitlab.vilmosnagy.snakeyaml.parser.services

import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlCollection
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlElement
import com.gitlab.vilmosnagy.snakeyaml.parser.utils.findUniqueOrNull
import java.lang.IllegalStateException
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.reflect.*
import kotlin.reflect.full.memberProperties

data class AnnotationParsedElement(
        val elementName: String,
        val property: KProperty<*>
)

data class AnnotationParsedClass<RESULT_TYPE>(
        val constructor: KFunction<RESULT_TYPE>?,
        val fields: List<AnnotationParsedElement>
)

@Singleton
class AnnotationParser
@Inject constructor(

) {

    fun <RESULT_TYPE: Any> parseAnnotationsOnClass(klass: KClass<RESULT_TYPE>): AnnotationParsedClass<RESULT_TYPE> {
        val constructor = findAnnotatedConstructorForClass(klass)
        val fields = constructor?.let { parseParametersFromConstructor(klass, constructor) } ?: emptyList()
        return AnnotationParsedClass(constructor, fields)
    }

    private fun <RESULT_TYPE: Any> parseParametersFromConstructor(klass: KClass<RESULT_TYPE>, constructor: KFunction<RESULT_TYPE>): List<AnnotationParsedElement> {
        val parametersIndexed = constructor.parameters.mapIndexed { index, kParameter -> kParameter.name to index }.toMap()
        return klass
                .memberProperties
                .map { it -> it to parametersIndexed[it.name] }
                .sortedBy { it.second }
                .map { it.first }
                .map { parseParameter(it) }
    }

    private fun parseParameter(property: KProperty<*>): AnnotationParsedElement {
        val elementName = when {
            property.isAnnotationPresent(YamlElement::class) -> property.getAnnotation(YamlElement::class)!!.name
            property.isAnnotationPresent(YamlCollection::class) -> property.getAnnotation(YamlCollection::class)!!.name
            else -> throw IllegalStateException("The parameter (${property.name}) is not annotated with the `YamlElement` or the `YamlCollection` annotation")
        }

        return AnnotationParsedElement(elementName, property)
    }

    private fun <RESULT_TYPE: Any> findAnnotatedConstructorForClass(klass: KClass<RESULT_TYPE>): KFunction<RESULT_TYPE>? {
        return klass
                .constructors
                .findUniqueOrNull(
                        errorMessage = "More than one constructor is annotated with the `YamlConstructor` annotation on Class: ${klass.qualifiedName}!"
                ) { it.annotations.any { it is YamlConstructor } }
    }

}

internal inline fun <reified T: Annotation> KAnnotatedElement.getAnnotation(klass: KClass<T>): T? {
    return annotations.firstOrNull { klass.java.isInstance(it) } as? T
}

internal fun KAnnotatedElement.isAnnotationPresent(klass: KClass<out Annotation>): Boolean {
    return annotations.any { klass.java.isInstance(it) }
}
