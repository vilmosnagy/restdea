package com.gitlab.vilmosnagy.snakeyaml.parser.annotations

import com.gitlab.vilmosnagy.snakeyaml.parser.interfaces.YamlObjectTypeProvider
import com.gitlab.vilmosnagy.snakeyaml.parser.interfaces.YamlObjectFactory
import kotlin.reflect.KClass

interface YamlNamed

@Target(AnnotationTarget.CONSTRUCTOR)
@Retention(AnnotationRetention.RUNTIME)
annotation class YamlConstructor

@Target(AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.RUNTIME)
annotation class YamlElement(
        val name: String
)

@Target(AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.RUNTIME)
annotation class YamlCollection(
        val name: String
)

@Target(AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.RUNTIME)
annotation class YamlTypeProvider(
        val typeProviderClass: KClass<out YamlObjectTypeProvider<*>>
)

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class YamlFactory(
        val factoryClass: KClass<out YamlObjectFactory<*>>
)