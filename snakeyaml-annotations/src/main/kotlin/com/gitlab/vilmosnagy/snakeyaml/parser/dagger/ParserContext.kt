package com.gitlab.vilmosnagy.snakeyaml.parser.dagger

import com.gitlab.vilmosnagy.snakeyaml.parser.YamlParser
import com.gitlab.vilmosnagy.snakeyaml.parser.YamlSerializer
import dagger.Component
import org.yaml.snakeyaml.Yaml
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ParserModule::class))
interface ParserContext {

    companion object {
        val INSTANCE: ParserContext = DaggerParserContext.builder().build()
    }

    val yamlParser: YamlParser
    val yamlSerializer: YamlSerializer

}
