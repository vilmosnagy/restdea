package com.gitlab.vilmosnagy.snakeyaml.parser.mappers

import com.gitlab.vilmosnagy.snakeyaml.parser.services.EnumParser
import kotlin.reflect.KClass

internal object DoubleTypeMapper : TypeMapper<Double> {
    override fun getType() = Double::class
    override fun convertToType(klass: KClass<Double>, obj: Any) = obj.toString().toDouble()
}

internal object FloatTypeMapper : TypeMapper<Float> {
    override fun getType() = Float::class
    override fun convertToType(klass: KClass<Float>, obj: Any) = obj.toString().toFloat()
}

internal object IntegerTypeMapper : TypeMapper<Int> {
    override fun getType() = Int::class
    override fun convertToType(klass: KClass<Int>, obj: Any) = obj.toString().toInt()
}

internal object LongTypeMapper : TypeMapper<Long> {
    override fun getType() = Long::class
    override fun convertToType(klass: KClass<Long>, obj: Any) = obj.toString().toLong()
}

internal object ByteTypeMapper : TypeMapper<Byte> {
    override fun getType() = Byte::class
    override fun convertToType(klass: KClass<Byte>, obj: Any) = obj.toString().toByte()
}

internal object ShortTypeMapper : TypeMapper<Short> {
    override fun getType() = Short::class
    override fun convertToType(klass: KClass<Short>, obj: Any) = obj.toString().toShort()
}

internal object CharacterTypeMapper : TypeMapper<Char> {
    override fun getType() = Char::class
    override fun convertToType(klass: KClass<Char>, obj: Any) = obj.toString().first()
}

internal object StringTypeMapper : TypeMapper<String> {
    override fun getType() = String::class
    override fun convertToType(klass: KClass<String>, obj: Any) = obj.toString()
}

internal object EnumTypeMapper : TypeMapper<Enum<*>> {
    override fun getType() = Enum::class
    override fun convertToType(klass: KClass<Enum<*>>, obj: Any): Enum<*> = EnumParser.enumValueOf(klass.java, obj.toString())
}