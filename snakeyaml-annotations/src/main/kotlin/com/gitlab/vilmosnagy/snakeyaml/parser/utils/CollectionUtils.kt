package com.gitlab.vilmosnagy.snakeyaml.parser.utils

import java.lang.IllegalStateException

inline fun <T> Iterable<T>.findUniqueOrNull(errorMessage: String? = null, predicate: (T) -> Boolean): T? {
    val filtered = filter { predicate(it) }
    return when {
        filtered.isEmpty() -> null
        filtered.size > 1 -> throw IllegalStateException(errorMessage ?: "More than one match in collection.")
        else -> filtered[0]
    }
}

inline fun <T> Array<T>.findUniqueOrNull(errorMessage: String? = null, predicate: (T) -> Boolean): T? {
    val filtered = filter { predicate(it) }
    return when {
        filtered.isEmpty() -> null
        filtered.size > 1 -> throw IllegalStateException(errorMessage ?: "More than one match in collection.")
        else -> filtered[0]
    }
}