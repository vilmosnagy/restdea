package com.gitlab.vilmosnagy.snakeyaml.parser.interfaces

import kotlin.reflect.KClass

interface YamlObjectTypeProvider<BASE_TYPE: Any> {

    fun provideResultClass(yamlAsMap: Map<String, Any>): KClass<out BASE_TYPE>

}