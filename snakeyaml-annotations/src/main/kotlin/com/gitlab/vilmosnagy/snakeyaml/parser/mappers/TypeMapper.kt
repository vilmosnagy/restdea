package com.gitlab.vilmosnagy.snakeyaml.parser.mappers

import com.gitlab.vilmosnagy.snakeyaml.parser.utils.findUniqueOrNull
import kotlin.reflect.KClass
import kotlin.reflect.full.isSuperclassOf

internal val TYPE_MAPPERS: List<TypeMapper<*>> = listOf(
        DoubleTypeMapper,
        FloatTypeMapper,
        IntegerTypeMapper,
        LongTypeMapper,
        ByteTypeMapper,
        ShortTypeMapper,
        CharacterTypeMapper,
        StringTypeMapper,
        EnumTypeMapper,
        SetTypeMapper,
        ListTypeMapper
)

internal fun <T: Any> getTypeMapperForClass(klass: KClass<T>): TypeMapper<T>? {
    return TYPE_MAPPERS
            .findUniqueOrNull { it.getType().isSuperclassOf(klass) }
            as TypeMapper<T>?
}

internal interface TypeMapper<T : Any> {

    fun getType(): KClass<T>

    fun convertToType(klass: KClass<T>, obj: Any): T

}