package com.gitlab.vilmosnagy.snakeyaml.parser.interfaces

interface YamlObjectFactory<BASE_TYPE: Any> {

    fun createObject(yamlAsMap: Map<String, Any>): BASE_TYPE
    fun serializeObject(obj: BASE_TYPE?): Map<String, Any>

}