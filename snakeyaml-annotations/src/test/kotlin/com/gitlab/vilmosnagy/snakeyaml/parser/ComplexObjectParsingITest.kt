package com.gitlab.vilmosnagy.snakeyaml.parser

import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlElement
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlTypeProvider
import com.gitlab.vilmosnagy.snakeyaml.parser.dagger.ParserContext
import com.gitlab.vilmosnagy.snakeyaml.parser.interfaces.YamlObjectTypeProvider
import org.intellij.lang.annotations.Language
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.reflect.KClass

internal class ComplexObjectParsingITest {

    val testObj = ParserContext.INSTANCE.yamlParser

    @Test
    fun should_parse_complex_object_automatically() {
        data class Foo @YamlConstructor constructor(
                @YamlElement(name = "s")
                val s: String
        )

        data class Bar @YamlConstructor constructor(
                @YamlElement(name = "foo")
                val foo: Foo
        )

        @Language("yaml")
        val yamlContent = """
            foo:
              s: string
            """.trimIndent()

        val parsedObj = testObj.parseYaml(yamlContent, Bar::class)
        assertEquals("string", parsedObj.foo.s)
    }

    @Test
    fun should_support_custom_object_mappers_when_the_object_type_is_determined_by_a_field() {
        abstract class Common
        data class Foo @YamlConstructor constructor(
                @YamlElement(name = "s")
                val s: String
        ) : Common()

        data class Bar @YamlConstructor constructor(
                @YamlElement(name = "i")
                val i: Int
        ) : Common()

        class FooBarConverter : YamlObjectTypeProvider<Common> {
            override fun provideResultClass(yamlAsMap: Map<String, Any>): KClass<out Common> {
                return if (yamlAsMap["type"] == "bar") Bar::class else Foo::class
            }
        }

        data class Parent @YamlConstructor constructor(
                @YamlElement(name = "common")
                @YamlTypeProvider(typeProviderClass = FooBarConverter::class)
                val common: Common
        )

        val yamlWithBarAsString = """
            common:
              type: bar
              i: 5
            """.trimIndent()
        val parsedWithBar = testObj.parseYaml(yamlWithBarAsString, Parent::class)
        assertEquals(5, (parsedWithBar.common as? Bar)?.i)
    }

    @Test
    fun should_not_fail_when_a_nullable_parameter_is_not_present_in_the_yaml() {
        data class Pojo @YamlConstructor constructor(
                @YamlElement(name = "s1")
                val s1: String?,

                @YamlElement(name = "s2")
                val s2: String?
        )

        val yamlAsString = """
            s2: foobar
            """.trimIndent()
        val parsed = testObj.parseYaml(yamlAsString, Pojo::class)
        assertEquals(Pojo(null, "foobar"), parsed)
    }

}