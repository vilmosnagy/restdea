package com.gitlab.vilmosnagy.snakeyaml.serializer

import com.gitlab.vilmosnagy.snakeyaml.get
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlCollection
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlFactory
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlTypeProvider
import com.gitlab.vilmosnagy.snakeyaml.parser.dagger.ParserContext
import com.gitlab.vilmosnagy.snakeyaml.parser.interfaces.YamlObjectFactory
import com.gitlab.vilmosnagy.snakeyaml.parser.interfaces.YamlObjectTypeProvider
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.yaml.snakeyaml.Yaml
import kotlin.reflect.KClass

class BasePojoTypeProvider : YamlObjectTypeProvider<BasePojo> {
    override fun provideResultClass(yamlAsMap: Map<String, Any>): KClass<out BasePojo> {
        return Pojo::class
    }
}

class PojoFactory : YamlObjectFactory<Pojo> {
    override fun serializeObject(obj: Pojo?): Map<String, Any> {
        return if (obj == null) emptyMap()
        else mapOf(
                "type" to "basePojo",
                "string_value" to obj.string
        )
    }

    override fun createObject(yamlAsMap: Map<String, Any>): Pojo {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

abstract class BasePojo

@YamlFactory(PojoFactory::class)
data class Pojo constructor(val string: String) : BasePojo()

data class Container @YamlConstructor constructor(

        @YamlCollection("list")
        @YamlTypeProvider(BasePojoTypeProvider::class)
        val pojoList: List<Pojo>
)


class CollectionWithFactoryDumpingTest {

    val testObj = ParserContext.INSTANCE.yamlSerializer

    @Test
    fun should_dump_lists_with_type_provider_and_factories_correcly() {
        val container = Container(listOf(Pojo("foo"), Pojo("bar")))
        val asString = testObj.serializeYaml(container)
        val reParsedToMap = Yaml().load<Map<String, Any>>(asString)
        Assertions.assertEquals("foo", reParsedToMap["list"][0]["string_value"])
        Assertions.assertEquals("basePojo", reParsedToMap["list"][0]["type"])
        Assertions.assertEquals("bar", reParsedToMap["list"][1]["string_value"])
        Assertions.assertEquals("basePojo", reParsedToMap["list"][1]["type"])
    }
}