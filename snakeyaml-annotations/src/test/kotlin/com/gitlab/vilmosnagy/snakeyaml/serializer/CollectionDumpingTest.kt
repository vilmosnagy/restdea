package com.gitlab.vilmosnagy.snakeyaml.serializer

import com.gitlab.vilmosnagy.snakeyaml.get
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlCollection
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlElement
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlTypeProvider
import com.gitlab.vilmosnagy.snakeyaml.parser.dagger.ParserContext
import com.gitlab.vilmosnagy.snakeyaml.parser.interfaces.YamlObjectTypeProvider
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.yaml.snakeyaml.Yaml

class CollectionDumpingTeqst {

    val testObj = ParserContext.INSTANCE.yamlSerializer

    @Test
    fun should_dump_simple_list_correctly() {
        data class Pojo @YamlConstructor constructor(@YamlElement(name = "string") val string: String)
        data class Container @YamlConstructor constructor(@YamlCollection("list") val pojoList: List<Pojo>)

        val container = Container(listOf(Pojo("foo"), Pojo("bar")))
        val asString = testObj.serializeYaml(container)
        val reParsedToMap = Yaml().load<Map<String, Any>>(asString)
        assertEquals("foo", reParsedToMap["list"][0]["string"])
        assertEquals("bar", reParsedToMap["list"][1]["string"])
    }

    @Test
    fun should_not_serialize_anything_on_empty_collection() {
        data class Pojo @YamlConstructor constructor(@YamlElement(name = "string") val string: String)
        data class Container @YamlConstructor constructor(@YamlCollection("list") val pojoList: List<Pojo>)
        val container = Container(emptyList())
        val asString = testObj.serializeYaml(container)
        val reParsedToMap = Yaml().load<Map<String, Any>>(asString)
        assertEquals(0, (reParsedToMap["list"] as Collection<*>).size)
    }
}


