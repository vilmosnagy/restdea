package com.gitlab.vilmosnagy.snakeyaml.serializer

import com.gitlab.vilmosnagy.snakeyaml.get
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlCollection
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlElement
import com.gitlab.vilmosnagy.snakeyaml.parser.dagger.ParserContext
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.yaml.snakeyaml.Yaml

class CompositePojoDumpingTest {

    val testObj = ParserContext.INSTANCE.yamlSerializer

    @Test
    fun should_dump_pojo_with_pojo_inside_correctly() {
        data class Pojo @YamlConstructor constructor(@YamlElement(name = "string") val string: String)
        data class Container @YamlConstructor constructor(@YamlCollection("pojo") val pojo: Pojo)

        val container = Container(Pojo("foobar"))
        val asString = testObj.serializeYaml(container)
        val reParsedToMap = Yaml().load<Map<String, Any>>(asString)
        Assertions.assertEquals("foobar", reParsedToMap["pojo"]["string"])
    }
}