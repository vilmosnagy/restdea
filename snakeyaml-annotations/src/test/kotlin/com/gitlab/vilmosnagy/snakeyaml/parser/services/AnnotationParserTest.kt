package com.gitlab.vilmosnagy.snakeyaml.parser.services

import com.gitlab.vilmosnagy.snakeyaml.parser.SimpleStringPojo
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlElement
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import kotlin.reflect.full.primaryConstructor

internal class AnnotationParserTest {

    val testObj = AnnotationParser()

    @Test
    fun should_parse_simple_string_pojo() {
        data class TestPojo @YamlConstructor constructor(
                @YamlElement(name = "str")
                val string: String
        )

        val parsedData = testObj.parseAnnotationsOnClass(TestPojo::class)
        val constructor = TestPojo::class.primaryConstructor!!
        assertEquals(constructor, parsedData.constructor)
        assertEquals(listOf(AnnotationParsedElement("str", TestPojo::string)), parsedData.fields)
    }
}