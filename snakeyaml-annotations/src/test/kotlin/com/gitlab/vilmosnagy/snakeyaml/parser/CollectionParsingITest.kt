package com.gitlab.vilmosnagy.snakeyaml.parser

import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlCollection
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlElement
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlTypeProvider
import com.gitlab.vilmosnagy.snakeyaml.parser.dagger.ParserContext
import org.intellij.lang.annotations.Language
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*
import kotlin.reflect.KClass

internal class CollectionParsingITest {

    val testObj = ParserContext.INSTANCE.yamlParser

    @Test
    fun should_be_able_to_parse_yaml_list_into_java_list() {
        data class Foo @YamlConstructor constructor(
                @YamlElement(name = "s")
                val s: String
        )

        data class Bar @YamlConstructor constructor(
                @YamlCollection(name = "foos")
                val foos: List<Foo>
        )

        @Language("yaml")
        val yamlContent = """
            foos:
              - s: s1
              - s: s2
              - s: s3
            """.trimIndent()

        val parsedObj = testObj.parseYaml(yamlContent, Bar::class)
        assertEquals(Bar(listOf(Foo("s1"), Foo("s2"), Foo("s3"))), parsedObj)
    }

    @Test
    fun should_be_able_to_parse_yaml_list_into_java_set() {
        data class Foo @YamlConstructor constructor(
                @YamlElement(name = "s")
                val s: String
        )

        data class Bar @YamlConstructor constructor(
                @YamlCollection(name = "foos")
                val foos: Set<Foo>
        )

        @Language("yaml")
        val yamlContent = """
            foos:
              - s: s1
              - s: s2
              - s: s3
            """.trimIndent()

        val parsedObj = testObj.parseYaml(yamlContent, Bar::class)
        assertEquals(Bar(setOf(Foo("s1"), Foo("s2"), Foo("s3"))), parsedObj)
    }

    @Test
    fun should_provide_empty_collection_instead_of_null_when_the_collection_is_not_present_in_the_yaml_file() {
        data class Foo @YamlConstructor constructor(
                @YamlElement(name = "s")
                val s: String
        )

        data class Bar @YamlConstructor constructor(
                @YamlElement(name = "name")
                val name: String,

                @YamlCollection(name = "foos")
                val foos: List<Foo>
        )

        @Language("yaml")
        val yamlContent = """
            name: fooBar
            """.trimIndent()

        val parsedObj = testObj.parseYaml(yamlContent, Bar::class)
        assertEquals(Bar("fooBar", emptyList()), parsedObj)
    }

    @Test
    fun should_be_able_to_parse_yaml_list_into_java_special_list() {
        data class Foo @YamlConstructor constructor(
                @YamlElement(name = "s")
                val s: String
        )

        data class Bar @YamlConstructor constructor(
                @YamlCollection(name = "foos")
                val foos: LinkedList<Foo>
        )

        @Language("yaml")
        val yamlContent = """
            foos:
              - s: s1
              - s: s2
              - s: s3
            """.trimIndent()

        val parsedObj = testObj.parseYaml(yamlContent, Bar::class)
        assertEquals(Bar(LinkedList(listOf(Foo("s1"), Foo("s2"), Foo("s3")))), parsedObj)
    }

    @Test
    fun should_be_able_to_parse_yaml_list_into_java_special_set() {
        data class Foo @YamlConstructor constructor(
                @YamlElement(name = "s")
                val s: String
        )

        data class Bar @YamlConstructor constructor(
                @YamlCollection(name = "foos")
                val foos: LinkedHashSet<Foo>
        )

        @Language("yaml")
        val yamlContent = """
            foos:
              - s: s1
              - s: s2
              - s: s3
            """.trimIndent()

        val parsedObj = testObj.parseYaml(yamlContent, Bar::class)
        assertEquals(Bar(linkedSetOf(Foo("s1"), Foo("s2"), Foo("s3"))), parsedObj)
    }

    @Test
    fun should_be_able_to_parse_yaml_list_into_java_collection_with_converter() {
        abstract class Combined
        data class Bar @YamlConstructor constructor(
                @YamlElement(name = "i")
                val i: Int
        ) : Combined()

        data class Foo @YamlConstructor constructor(
                @YamlElement(name = "s")
                val s: String
        ) : Combined()

        class FooBarConverter : com.gitlab.vilmosnagy.snakeyaml.parser.interfaces.YamlObjectTypeProvider<Combined> {
            override fun provideResultClass(yamlAsMap: Map<String, Any>): KClass<out Combined> {
                return when (yamlAsMap["type"]) {
                    "foo" -> Foo::class
                    "bar" -> Bar::class
                    else -> TODO()
                }
            }

        }

        data class Parent @YamlConstructor constructor(
                @YamlCollection(name = "cs")
                @YamlTypeProvider(FooBarConverter::class)
                val foos: List<Combined>
        )

        @Language("yaml")
        val yamlContent = """
            cs:
              - type: foo
                s: string
              - type: bar
                i: 5
            """.trimIndent()


        val parsedObj = testObj.parseYaml(yamlContent, Parent::class)
        assertEquals(Parent(listOf(
                Foo("string"), Bar(5)
        )), parsedObj)
    }
}