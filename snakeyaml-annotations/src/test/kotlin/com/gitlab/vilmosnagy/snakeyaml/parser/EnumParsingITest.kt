package com.gitlab.vilmosnagy.snakeyaml.parser

import com.gitlab.vilmosnagy.snakeyaml.parser.EnumParsingITest.Enum.*
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlCollection
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlElement
import com.gitlab.vilmosnagy.snakeyaml.parser.dagger.ParserContext
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class EnumParsingITest {

    val testObj = ParserContext.INSTANCE.yamlParser

    enum class Enum {
        FOO,
        BAR,
        BAZ
    }

    @Test
    fun should_parse_enums_without_annotation_by_their_name() {
        data class EnumHolder @YamlConstructor constructor(
                @YamlElement(name = "enumValue")
                val enumValue: Enum
        )

        val yamlContent = """
            enumValue: FOO
            """.trimIndent()
        val parsed = testObj.parseYaml(yamlContent, EnumHolder::class)
        assertEquals(EnumHolder(FOO), parsed)
    }

    @Test
    fun should_parse_enums_without_annotation_in_collections_by_their_name() {
        data class EnumHolder @YamlConstructor constructor(
                @YamlCollection(name = "enums")
                val enums: List<Enum>
        )

        val yamlContent = """
            enums:
             - FOO
             - FOO
             - BAZ
             - BAR
            """.trimIndent()
        val parsed = testObj.parseYaml(yamlContent, EnumHolder::class)
        assertEquals(EnumHolder(listOf(FOO, FOO, BAZ, BAR)), parsed)
    }

}