package com.gitlab.vilmosnagy.snakeyaml.parser

import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlElement
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlFactory
import com.gitlab.vilmosnagy.snakeyaml.parser.dagger.ParserContext
import com.gitlab.vilmosnagy.snakeyaml.parser.interfaces.YamlObjectFactory
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class YamlObjectFactoryITest {

    val testObj = ParserContext.INSTANCE.yamlParser

    companion object {
        var factoryParameterMap: Map<String, Any> = emptyMap()
    }

    @BeforeEach
    fun clearCachedMap() {
        factoryParameterMap = emptyMap()
    }

    class DetailFactory : YamlObjectFactory<Detail> {
        override fun serializeObject(obj: Detail?): Map<String, Any> {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun createObject(yamlAsMap: Map<String, Any>): Detail {
            factoryParameterMap = yamlAsMap
            return Detail("foobar")
        }
    }

    @YamlFactory(DetailFactory::class)
    data class Detail(val s: String)

    data class Master @YamlConstructor constructor(
            @YamlElement("detail")
            val detail: Detail
    )

    @Test
    fun should_call_custom_factory_if_annotation_is_present_on_a_fields_class() {
        val yamlContentAsString = """
            detail:
             foo: bar
             bar: xyz
            """.trimIndent()

        val parsedElement = testObj.parseYaml(yamlContentAsString, Master::class)
        assertEquals(Master(Detail("foobar")), parsedElement)
        assertEquals(mapOf("foo" to "bar", "bar" to "xyz"), factoryParameterMap)
    }

    @Test
    fun should_call_custom_factory_if_annotation_is_present_on_a_root_class() {
        val yamlContentAsString = """
            foo: xyz
            bar: baz
            """.trimIndent()

        val parsedElement = testObj.parseYaml(yamlContentAsString, Detail::class)
        assertEquals(Detail("foobar"), parsedElement)
        assertEquals(mapOf("foo" to "xyz", "bar" to "baz"), factoryParameterMap)
    }
}