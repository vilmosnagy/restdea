package com.gitlab.vilmosnagy.snakeyaml.serializer

import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlElement
import com.gitlab.vilmosnagy.snakeyaml.parser.dagger.ParserContext
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.yaml.snakeyaml.Yaml

class PrimitiveValueDumpingTest {

    val testObj = ParserContext.INSTANCE.yamlSerializer

    @Test
    fun should_dump_single_string_into_correct_yaml_representation() {
        data class Pojo @YamlConstructor constructor(@YamlElement(name = "string") val string: String)

        val pojo = Pojo("fooBar")
        val asString = testObj.serializeYaml(pojo)
        val reParsedToMap = Yaml().load<Map<String, Any>>(asString)
        assertEquals(mapOf("string" to "fooBar"), reParsedToMap)
    }

    @Test
    fun should_dump_multiple_string_into_correct_yaml_representation() {
        data class Pojo @YamlConstructor constructor(
                @YamlElement(name = "string1") val string1: String,
                @YamlElement(name = "string2") val string2: String
        )

        val pojo = Pojo("fooBar", "barXyz")
        val asString = testObj.serializeYaml(pojo)
        val reParsedToMap = Yaml().load<Map<String, Any>>(asString)
        assertEquals("fooBar", reParsedToMap["string1"])
        assertEquals("barXyz", reParsedToMap["string2"])
    }

    @Test
    fun should_dump_multiple_string_number_bool_into_correct_yaml_representation() {
        data class Pojo @YamlConstructor constructor(
                @YamlElement(name = "string") val string: String,
                @YamlElement(name = "int") val int: Int?,
                @YamlElement(name = "double") val double: Double,
                @YamlElement(name = "bool") val boolean: Boolean
        )

        val pojo = Pojo("fooBar", null, 5.1, false)
        val asString = testObj.serializeYaml(pojo)
        val reParsedToMap = Yaml().load<Map<String, Any>>(asString)
        assertEquals("fooBar", reParsedToMap["string"])
        assertEquals(null, reParsedToMap["int"])
        assertEquals(5.1, reParsedToMap["double"] as Double, 0.1)
        assertEquals(false, reParsedToMap["bool"])
    }
}