package com.gitlab.vilmosnagy.snakeyaml.parser

import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlElement
import com.gitlab.vilmosnagy.snakeyaml.parser.dagger.ParserContext
import org.intellij.lang.annotations.Language
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


internal data class SimpleStringPojo @YamlConstructor constructor(
        @YamlElement(name = "str")
        val string: String
)

internal data class SimpleCharPojo @YamlConstructor constructor(
        @YamlElement(name = "char-obj")
        val charObj: Char?,

        @YamlElement(name = "char-prim")
        val charPrim: Char
)

internal class SimpleStringParsingITest {

    val testObj = ParserContext.INSTANCE.yamlParser

    @Test
    fun should_parse_char_primitive_correctly() {
        @Language("yaml")
        val yaml = """
            char-obj: a
            char-prim: a
            """.trimIndent()

        val parsed = testObj.parseYaml(yaml, SimpleCharPojo::class)
        assertEquals('a', parsed.charObj)
        assertEquals('a', parsed.charPrim)
    }

    @Test
    fun should_parse_single_line_string() {
        @Language("yaml")
        val yaml = """
            str: some string value
            """.trimIndent()

        val parsed = testObj.parseYaml(yaml, SimpleStringPojo::class)
        assertEquals("some string value", parsed.string)
    }

    @Test
    fun should_parse_multi_line_string_without_newlines() {
        @Language("yaml")
        val yaml = """
            str: >
              some
              string
              value
            """.trimIndent()

        val parsed = testObj.parseYaml(yaml, SimpleStringPojo::class)
        assertEquals("some string value", parsed.string)
    }

    @Test
    fun should_parse_multi_line_string_with_newlines() {
        @Language("yaml")
        val yaml = """
            str: |
              some
              string
              value
            """.trimIndent()

        val parsed = testObj.parseYaml(yaml, SimpleStringPojo::class)
        assertEquals("some\nstring\nvalue", parsed.string)
    }
}