package com.gitlab.vilmosnagy.snakeyaml.serializer

import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlElement
import com.gitlab.vilmosnagy.snakeyaml.parser.dagger.ParserContext
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.yaml.snakeyaml.Yaml

class EnumDumpingTest {

    val testObj = ParserContext.INSTANCE.yamlSerializer

    enum class Letter { A, B, C, D }
    enum class LetterWithToString { A, B, C, D;

        override fun toString(): String {
            return super.toString() + "salt"
        }
    }

    @Test
    fun should_dump_enums_correctly() {
        data class EnumPojo @YamlConstructor constructor(@YamlElement("letter") val letter: Letter)

        val pojo = EnumPojo(Letter.A)
        val asString = testObj.serializeYaml(pojo)
        val reParsedToMap = Yaml().load<Map<String, Any>>(asString)
        assertEquals("A", reParsedToMap["letter"])
    }

    @Test
    fun should_dump_enums_with_extra_code_correctly() {
        data class EnumPojo @YamlConstructor constructor(@YamlElement("letter") val letter: LetterWithToString)

        val pojo = EnumPojo(LetterWithToString.C)
        val asString = testObj.serializeYaml(pojo)
        val reParsedToMap = Yaml().load<Map<String, Any>>(asString)
        assertEquals("C", reParsedToMap["letter"])
    }
}