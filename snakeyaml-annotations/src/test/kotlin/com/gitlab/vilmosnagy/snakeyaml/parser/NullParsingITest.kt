package com.gitlab.vilmosnagy.snakeyaml.parser

import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlElement
import com.gitlab.vilmosnagy.snakeyaml.parser.dagger.ParserContext
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test


internal class NullParsingITest {

    val testObj = ParserContext.INSTANCE.yamlParser

    @Test
    fun should_parse_pojos_without_error_when_content_is_nullable_and_string_is_empty() {
        data class NullablePojo @YamlConstructor constructor(
                @YamlElement(name = "string")
                val string: String?
        )

        val yamlContent = ""
        val parsed = testObj.parseYaml(yamlContent, NullablePojo::class)
        Assertions.assertEquals(NullablePojo(null), parsed)
    }

    @Test
    fun should_call_default_constructor_if_class_has_any_and_its_nullable_in_parent() {
        data class ChildPojo @YamlConstructor constructor(
                @YamlElement(name = "string")
                val string: String
        ) {
            constructor(): this("##deafult##")
        }

        data class ParentPojo @YamlConstructor constructor(
                @YamlElement(name = "child")
                val child: ChildPojo
        )

        val yamlContent = ""
        val parsed = testObj.parseYaml(yamlContent, ParentPojo::class)
        Assertions.assertEquals(ParentPojo(ChildPojo()), parsed)
    }


}