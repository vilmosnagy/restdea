package com.gitlab.vilmosnagy.snakeyaml.parser

import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlConstructor
import com.gitlab.vilmosnagy.snakeyaml.parser.annotations.YamlElement
import com.gitlab.vilmosnagy.snakeyaml.parser.dagger.ParserContext
import org.intellij.lang.annotations.Language
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


internal data class SimpleNumberPojo @YamlConstructor constructor(
        @YamlElement(name = "float-obj")
        val floatObj: Float?,

        @YamlElement(name = "float-prim")
        val floatPrim: Float,

        @YamlElement(name = "double-obj")
        val doubleObj: Double?,

        @YamlElement(name = "double-prim")
        val doublePrim: Double,

        @YamlElement(name = "long-obj")
        val longObj: Long?,

        @YamlElement(name = "long-prim")
        val longPrim: Long,

        @YamlElement(name = "int-obj")
        val intObj: Int?,

        @YamlElement(name = "int-prim")
        val intPrim: Int,

        @YamlElement(name = "byte-obj")
        val byteObj: Byte?,

        @YamlElement(name = "byte-prim")
        val bytePrim: Byte,

        @YamlElement(name = "short-obj")
        val shortObj: Byte?,

        @YamlElement(name = "short-prim")
        val shortPrim: Byte
)

internal class SimpleNumberParsingITest {

    val testObj = ParserContext.INSTANCE.yamlParser

    @Test
    fun should_parse_numbers_correctly_if_every_number_is_integer() {
        @Language("yaml")
        val yamlContent = """
            float-obj: 1
            float-prim: 1
            double-obj: 1
            double-prim: 1
            long-obj: 1
            long-prim: 1
            int-obj: 1
            int-prim: 1
            byte-obj: 1
            byte-prim: 1
            short-obj: 1
            short-prim: 1
            """.trimIndent()

        val parsedObj = testObj.parseYaml(yamlContent, SimpleNumberPojo::class)
        assertEquals(1.0F, parsedObj.floatObj)
        assertEquals(1.0F, parsedObj.floatPrim)
        assertEquals(1.0, parsedObj.doubleObj)
        assertEquals(1.0, parsedObj.doublePrim)
        assertEquals(1L, parsedObj.longObj)
        assertEquals(1L, parsedObj.longPrim)
        assertEquals(1, parsedObj.intObj)
        assertEquals(1, parsedObj.intPrim)
    }
}